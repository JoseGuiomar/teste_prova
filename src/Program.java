public class Program {
    public static void main(String[] args) {
    Book books = new Book() {

        @Override
        public void display() {

        }
    };
    Item item = new Item() {

        @Override
        public void display() {

        }
    };

    books.setAuthor("  Nome do autor : Bruno Walker");
    books.setEdition("Número da Edição : 5");
    books.setVolume("Volume Nº 3");

    item.setTitle("Titulo do Livro: O Windows que nunca quis  ");
    item.setPublished("Editora: Linux  ");
    item.setYearPublished("Ano de publicação:  2021 ");
    item.setIsbn("Código Nº : 789325733");
    item.setPrice("R$: 100,00");

    System.out.println(books.getAuthor());
    System.out.println(books.getEdition());
    System.out.println(books.getVolume());

    System.out.println(item.getTitle());
    System.out.println(item.getPublished());
    System.out.println(item.getYearPublished());
    System.out.println(item.getIsbn());
    System.out.println(item.getPrice());


    }

}
